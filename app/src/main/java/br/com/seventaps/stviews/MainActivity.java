package br.com.seventaps.stviews;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {
    // EditText selected;
    Context context;
    TreeNavigationView treeNavigationView;
    TreeNavigationView treeNavigationView2;
    TreeNavigationView treeNavigationView3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        treeNavigationView = (TreeNavigationView)findViewById(R.id.tree_nav);
        treeNavigationView2 = (TreeNavigationView)findViewById(R.id.tree_nav2);
        treeNavigationView3 = (TreeNavigationView)findViewById(R.id.tree_nav3);

        ArrayList<EliseusItem> items = new ArrayList<>();
        items.add( new EliseusItem("Acesso::Acesso Remoto (Terminal Services)::Bloquear", "acesso__acesso_remoto__terminal_services___bloquear"));
        items.add( new EliseusItem("Acesso::Acesso Remoto (Terminal Services)::Configurar", "acesso__acesso_remoto__terminal_services___configurar"));
        items.add( new EliseusItem("Acesso::Acesso Remoto (Terminal Services)::Solicitar", "acesso__acesso_remoto__terminal_services___solicitar"));
        items.add( new EliseusItem("Acesso::BI::Bloquear", "acesso__bi__bloquear"));
        items.add( new EliseusItem("Acesso::BI::Configurar", "acesso__bi__configurar"));
        items.add( new EliseusItem("Acesso::BI::Solicitar", "acesso__bi__solicitar"));
        items.add( new EliseusItem("Acesso::E-mail::Bloquear usuário", "acesso__e_mail__bloquear_usuário"));
        items.add( new EliseusItem("Acesso::E-mail::Configurar", "acesso__e_mail__configurar"));
        items.add( new EliseusItem("Acesso::E-mail::Criar usuário", "acesso__e_mail__criar_usuário"));
        items.add( new EliseusItem("Acesso::Google Docs (Planilhas, Documentos)::Bloqueio de acesso", "acesso__google_docs__planilhas__documentos___bloqueio_de_acesso"));
        items.add( new EliseusItem("Acesso::Google Docs (Planilhas, Documentos)::Solicitação de acesso", "acesso__google_docs__planilhas__documentos___solicitação_de_acesso"));
        items.add( new EliseusItem("Acesso::Impressora::Bloqueio de acesso", "acesso__impressora__bloqueio_de_acesso"));
        items.add( new EliseusItem("Acesso::Impressora::Configurar", "acesso__impressora__configurar"));
        items.add( new EliseusItem("Acesso::Impressora::Solicitação de acesso", "acesso__impressora__solicitação_de_acesso"));
        items.add( new EliseusItem("Acesso::Painel uValet::Bloquear usuário", "acesso__painel_uvalet__bloquear_usuário"));
        items.add( new EliseusItem("Acesso::Painel uValet::Configurar perfil", "acesso__painel_uvalet__configurar_perfil"));
        items.add( new EliseusItem("Acesso::Painel uValet::Criar usuário", "acesso__painel_uvalet__criar_usuário"));
        items.add( new EliseusItem("Acesso::Park Solicitações (ZenDesk)::Bloquear usuário", "acesso__park_solicitações__zendesk___bloquear_usuário"));
        items.add( new EliseusItem("Acesso::Park Solicitações (ZenDesk)::Configurar perfil", "acesso__park_solicitações__zendesk___configurar_perfil"));
        items.add( new EliseusItem("Acesso::Park Solicitações (ZenDesk)::Criar usuário", "acesso__park_solicitações__zendesk___criar_usuário"));
        items.add( new EliseusItem("Acesso::Portal SGI (Qualidade)::Bloquear usuário", "acesso__portal_sgi__qualidade___bloquear_usuário"));
        items.add( new EliseusItem("Acesso::Portal SGI (Qualidade)::Configurar perfil", "acesso__portal_sgi__qualidade___configurar_perfil"));
        items.add( new EliseusItem("Acesso::Portal SGI (Qualidade)::Criar usuário", "acesso__portal_sgi__qualidade___criar_usuário"));
        items.add( new EliseusItem("Acesso::Rede WIFI::Bloqueio de acesso", "acesso__rede_wifi__bloqueio_de_acesso"));
        items.add( new EliseusItem("Acesso::Rede WIFI::Solicitação de acesso", "acesso__rede_wifi__solicitação_de_acesso"));
        items.add( new EliseusItem("Acesso::Rede::Bloquear usuário", "acesso__rede__bloquear_usuário"));
        items.add( new EliseusItem("Acesso::Rede::Configurar", "acesso__rede__configurar"));
        items.add( new EliseusItem("Acesso::Rede::Criar usuário", "acesso__rede__criar_usuário"));
        items.add( new EliseusItem("Acesso::Servidor de arquivos::Bloqueio de acesso", "acesso__servidor_de_arquivos__bloqueio_de_acesso"));
        items.add( new EliseusItem("Acesso::Servidor de arquivos::Mapeamento de pasta", "acesso__servidor_de_arquivos__mapeamento_de_pasta"));
        items.add( new EliseusItem("Acesso::Servidor de arquivos::Restaurar arquivos", "acesso__servidor_de_arquivos__restaurar_arquivos"));
        items.add( new EliseusItem("Acesso::Servidor de arquivos::Solicitação de acesso", "acesso__servidor_de_arquivos__solicitação_de_acesso"));
        items.add( new EliseusItem("Acesso::Sênior Rubi::Bloquear usuário", "acesso__sênior_rubi__bloquear_usuário"));
        items.add( new EliseusItem("Acesso::Sênior Rubi::Configurar perfil", "acesso__sênior_rubi__configurar_perfil"));
        items.add( new EliseusItem("Acesso::Sênior Rubi::Criar usuário", "acesso__sênior_rubi__criar_usuário"));
        items.add( new EliseusItem("Acesso::Sênior Sapiêns::Bloquear usuário", "acesso__sênior_sapiêns__bloquear_usuário"));
        items.add( new EliseusItem("Acesso::Sênior Sapiêns::Configurar perfil", "acesso__sênior_sapiêns__configurar_perfil"));
        items.add( new EliseusItem("Acesso::Sênior Sapiêns::Criar usuário", "acesso__sênior_sapiêns__criar_usuário"));
        items.add( new EliseusItem("Acesso::TeamViewer::Bloquear usuário", "acesso__teamviewer__bloquear_usuário"));
        items.add( new EliseusItem("Acesso::TeamViewer::Configurar perfil", "acesso__teamviewer__configurar_perfil"));
        items.add( new EliseusItem("Acesso::TeamViewer::Criar usuário", "acesso__teamviewer__criar_usuário"));
        items.add( new EliseusItem("Acesso::VPN::Bloquear", "acesso__vpn__bloquear"));
        items.add( new EliseusItem("Acesso::VPN::Configurar", "acesso__vpn__configurar"));
        items.add( new EliseusItem("Acesso::VPN::Solicitar", "acesso__vpn__solicitar"));
        items.add( new EliseusItem("Acesso::WPS Enterprise::Bloquear usuário", "acesso__wps_enterprise__bloquear_usuário"));
        items.add( new EliseusItem("Acesso::WPS Enterprise::Configurar perfil", "acesso__wps_enterprise__configurar_perfil"));
        items.add( new EliseusItem("Acesso::WPS Enterprise::Criar usuário", "acesso__wps_enterprise__criar_usuário"));
        items.add( new EliseusItem("Hardware::Caixas de som::Configurar", "hardware__caixas_de_som__configurar"));
        items.add( new EliseusItem("Hardware::Caixas de som::Solicitar", "hardware__caixas_de_som__solicitar"));
        items.add( new EliseusItem("Hardware::Caixas de som::Verificar", "hardware__caixas_de_som__verificar"));
        items.add( new EliseusItem("Hardware::Desktop::Configurar", "hardware__desktop__configurar"));
        items.add( new EliseusItem("Hardware::Desktop::Recolher", "hardware__desktop__recolher"));
        items.add( new EliseusItem("Hardware::Desktop::Remanejar", "hardware__desktop__remanejar"));
        items.add( new EliseusItem("Hardware::Desktop::Solicitar", "hardware__desktop__solicitar"));
        items.add( new EliseusItem("Hardware::Desktop::Verificar", "hardware__desktop__verificar"));
        items.add( new EliseusItem("Hardware::Impressora::Configurar", "hardware__impressora__configurar"));
        items.add( new EliseusItem("Hardware::Impressora::Solicitar", "hardware__impressora__solicitar"));
        items.add( new EliseusItem("Hardware::Impressora::Troca de toner", "hardware__impressora__troca_de_toner"));
        items.add( new EliseusItem("Hardware::Impressora::Verificar", "hardware__impressora__verificar"));
        items.add( new EliseusItem("Hardware::Mouse::Configurar", "hardware__mouse__configurar"));
        items.add( new EliseusItem("Hardware::Mouse::Solicitar", "hardware__mouse__solicitar"));
        items.add( new EliseusItem("Hardware::Mouse::Verificar", "hardware__mouse__verificar"));
        items.add( new EliseusItem("Hardware::Notebook::Configurar", "hardware__notebook__configurar"));
        items.add( new EliseusItem("Hardware::Notebook::Recolher", "hardware__notebook__recolher"));
        items.add( new EliseusItem("Hardware::Notebook::Remanejar", "hardware__notebook__remanejar"));
        items.add( new EliseusItem("Hardware::Notebook::Solicitar", "hardware__notebook__solicitar"));
        items.add( new EliseusItem("Hardware::Notebook::Verificar", "hardware__notebook__verificar"));
        items.add( new EliseusItem("Hardware::Servidor::Configurar", "hardware__servidor__configurar"));
        items.add( new EliseusItem("Hardware::Servidor::Implantar", "hardware__servidor__implantar"));
        items.add( new EliseusItem("Hardware::Servidor::Recolher", "hardware__servidor__recolher"));
        items.add( new EliseusItem("Hardware::Servidor::Solicitar", "hardware__servidor__solicitar"));
        items.add( new EliseusItem("Hardware::Servidor::Verificar", "hardware__servidor__verificar"));
        items.add( new EliseusItem("Hardware::Tablet::Configurar", "hardware__tablet__configurar"));
        items.add( new EliseusItem("Hardware::Tablet::Resetar", "hardware__tablet__resetar"));
        items.add( new EliseusItem("Hardware::Tablet::Solicitar", "hardware__tablet__solicitar"));
        items.add( new EliseusItem("Hardware::Tablet::Verificar", "hardware__tablet__verificar"));
        items.add( new EliseusItem("Hardware::Teclado::Configurar", "hardware__teclado__configurar"));
        items.add( new EliseusItem("Hardware::Teclado::Solicitar", "hardware__teclado__solicitar"));
        items.add( new EliseusItem("Hardware::Teclado::Verificar", "hardware__teclado__verificar"));
        items.add( new EliseusItem("Rede::Firewall::Alterar acesso", "rede__firewall__alterar_acesso"));
        items.add( new EliseusItem("Rede::Firewall::Bloquear acesso", "rede__firewall__bloquear_acesso"));
        items.add( new EliseusItem("Rede::Firewall::Liberar acesso", "rede__firewall__liberar_acesso"));
        items.add( new EliseusItem("Rede::Internet::Bloquear site", "rede__internet__bloquear_site"));
        items.add( new EliseusItem("Rede::Internet::Liberar site", "rede__internet__liberar_site"));
        items.add( new EliseusItem("Rede::Internet::Verificar", "rede__internet__verificar"));
        items.add( new EliseusItem("Rede::Modem 3G::Configurar", "rede__modem_3g__configurar"));
        items.add( new EliseusItem("Rede::Modem 3G::Verificar", "rede__modem_3g__verificar"));
        items.add( new EliseusItem("Rede::Modem 4G::Configurar", "rede__modem_4g__configurar"));
        items.add( new EliseusItem("Rede::Modem 4G::Verificar", "rede__modem_4g__verificar"));
        items.add( new EliseusItem("Rede::Roteador::Configurar", "rede__roteador__configurar"));
        items.add( new EliseusItem("Rede::Roteador::Solicitar", "rede__roteador__solicitar"));
        items.add( new EliseusItem("Rede::Roteador::Substituir", "rede__roteador__substituir"));
        items.add( new EliseusItem("Rede::Switch::Alterar VLAN", "rede__switch__alterar_vlan"));
        items.add( new EliseusItem("Rede::Switch::Configurar", "rede__switch__configurar"));
        items.add( new EliseusItem("Rede::Switch::Solicitar", "rede__switch__solicitar"));
        items.add( new EliseusItem("Rede::Switch::Substituir", "rede__switch__substituir"));
        items.add( new EliseusItem("Software::Adobe Creative Suite::Configurar", "software__adobe_creative_suite__configurar"));
        items.add( new EliseusItem("Software::Adobe Creative Suite::Instalar", "software__adobe_creative_suite__instalar"));
        items.add( new EliseusItem("Software::Adobe Creative Suite::Verificar", "software__adobe_creative_suite__verificar"));
        items.add( new EliseusItem("Software::Adobe Photoshop::Configurar", "software__adobe_photoshop__configurar"));
        items.add( new EliseusItem("Software::Adobe Photoshop::Instalar", "software__adobe_photoshop__instalar"));
        items.add( new EliseusItem("Software::Adobe Photoshop::Verificar", "software__adobe_photoshop__verificar"));
        items.add( new EliseusItem("Software::Anti-vírus (Trend Micro)::Configurar", "software__anti_vírus__trend_micro___configurar"));
        items.add( new EliseusItem("Software::Anti-vírus (Trend Micro)::Instalar", "software__anti_vírus__trend_micro___instalar"));
        items.add( new EliseusItem("Software::Anti-vírus (Trend Micro)::Verificar", "software__anti_vírus__trend_micro___verificar"));
        items.add( new EliseusItem("Software::Autocad::Configurar", "software__autocad__configurar"));
        items.add( new EliseusItem("Software::Autocad::Instalar", "software__autocad__instalar"));
        items.add( new EliseusItem("Software::Autocad::Verificar", "software__autocad__verificar"));
        items.add( new EliseusItem("Software::Business Finance::Configurar", "software__business_finance__configurar"));
        items.add( new EliseusItem("Software::Business Finance::Instalar", "software__business_finance__instalar"));
        items.add( new EliseusItem("Software::Business Finance::Verificar", "software__business_finance__verificar"));
        items.add( new EliseusItem("Software::Compactador/descompactador de arquivos::Configurar", "software__compactador_descompactador_de_arquivos__configurar"));
        items.add( new EliseusItem("Software::Compactador/descompactador de arquivos::Instalar", "software__compactador_descompactador_de_arquivos__instalar"));
        items.add( new EliseusItem("Software::Compactador/descompactador de arquivos::Verificar", "software__compactador_descompactador_de_arquivos__verificar"));
        items.add( new EliseusItem("Software::Control ID REP iDX (Ponto Biométrico)::Configurar", "software__control_id_rep_idx__ponto_biométrico___configurar"));
        items.add( new EliseusItem("Software::Control ID REP iDX (Ponto Biométrico)::Instalar", "software__control_id_rep_idx__ponto_biométrico___instalar"));
        items.add( new EliseusItem("Software::Control ID REP iDX (Ponto Biométrico)::Verificar", "software__control_id_rep_idx__ponto_biométrico___verificar"));
        items.add( new EliseusItem("Software::Corel DRAW::Configurar", "software__corel_draw__configurar"));
        items.add( new EliseusItem("Software::Corel DRAW::Instalar", "software__corel_draw__instalar"));
        items.add( new EliseusItem("Software::Corel DRAW::Verificar", "software__corel_draw__verificar"));
        items.add( new EliseusItem("Software::EMS (CFTV)::Configurar", "software__ems__cftv___configurar"));
        items.add( new EliseusItem("Software::EMS (CFTV)::Instalar", "software__ems__cftv___instalar"));
        items.add( new EliseusItem("Software::EMS (CFTV)::Verificar", "software__ems__cftv___verificar"));
        items.add( new EliseusItem("Software::Flash::Atualizar", "software__flash__atualizar"));
        items.add( new EliseusItem("Software::Flash::Instalar", "software__flash__instalar"));
        items.add( new EliseusItem("Software::Flash::Verificar", "software__flash__verificar"));
        items.add( new EliseusItem("Software::Gmail (E-mail)::Configurar", "software__gmail__e_mail___configurar"));
        items.add( new EliseusItem("Software::Gmail (E-mail)::Verificar", "software__gmail__e_mail___verificar"));
        items.add( new EliseusItem("Software::Google Docs (Planilhas, Documentos)::Configurar", "software__google_docs__planilhas__documentos___configurar"));
        items.add( new EliseusItem("Software::Google Docs (Planilhas, Documentos)::Verificar", "software__google_docs__planilhas__documentos___verificar"));
        items.add( new EliseusItem("Software::Google Drive::Configurar", "software__google_drive__configurar"));
        items.add( new EliseusItem("Software::Google Drive::Verificar", "software__google_drive__verificar"));
        items.add( new EliseusItem("Software::Google Forms (Fomulários)::Configurar", "software__google_forms__fomulários___configurar"));
        items.add( new EliseusItem("Software::Google Forms (Fomulários)::Verificar", "software__google_forms__fomulários___verificar"));
        items.add( new EliseusItem("Software::Java::Atualizar", "software__java__atualizar"));
        items.add( new EliseusItem("Software::Java::Instalar", "software__java__instalar"));
        items.add( new EliseusItem("Software::Java::Verificar", "software__java__verificar"));
        items.add( new EliseusItem("Software::Libre Office::Configurar", "software__libre_office__configurar"));
        items.add( new EliseusItem("Software::Libre Office::Instalar", "software__libre_office__instalar"));
        items.add( new EliseusItem("Software::Libre Office::Verificar", "software__libre_office__verificar"));
        items.add( new EliseusItem("Software::Microsoft Access::Configurar", "software__microsoft_access__configurar"));
        items.add( new EliseusItem("Software::Microsoft Access::Instalar", "software__microsoft_access__instalar"));
        items.add( new EliseusItem("Software::Microsoft Access::Verificar", "software__microsoft_access__verificar"));
        items.add( new EliseusItem("Software::Microsoft Lync::Configurar", "software__microsoft_lync__configurar"));
        items.add( new EliseusItem("Software::Microsoft Lync::Instalar", "software__microsoft_lync__instalar"));
        items.add( new EliseusItem("Software::Microsoft Lync::Verificar", "software__microsoft_lync__verificar"));
        items.add( new EliseusItem("Software::Microsoft Office::Configurar", "software__microsoft_office__configurar"));
        items.add( new EliseusItem("Software::Microsoft Office::Instalar", "software__microsoft_office__instalar"));
        items.add( new EliseusItem("Software::Microsoft Office::Verificar", "software__microsoft_office__verificar"));
        items.add( new EliseusItem("Software::Microsoft Outlook::Configurar", "software__microsoft_outlook__configurar"));
        items.add( new EliseusItem("Software::Microsoft Outlook::Instalar", "software__microsoft_outlook__instalar"));
        items.add( new EliseusItem("Software::Microsoft Outlook::Verificar", "software__microsoft_outlook__verificar"));
        items.add( new EliseusItem("Software::Microsoft Visio::Configurar", "software__microsoft_visio__configurar"));
        items.add( new EliseusItem("Software::Microsoft Visio::Instalar", "software__microsoft_visio__instalar"));
        items.add( new EliseusItem("Software::Microsoft Visio::Verificar", "software__microsoft_visio__verificar"));
        items.add( new EliseusItem("Software::Navegador (Browser)::Configurar", "software__navegador__browser___configurar"));
        items.add( new EliseusItem("Software::Navegador (Browser)::Instalar", "software__navegador__browser___instalar"));
        items.add( new EliseusItem("Software::Navegador (Browser)::Verificar", "software__navegador__browser___verificar"));
        items.add( new EliseusItem("Software::Outros::Configurar", "software__outros__configurar"));
        items.add( new EliseusItem("Software::Outros::Homologar", "software__outros__homologar"));
        items.add( new EliseusItem("Software::Outros::Instalar", "software__outros__instalar"));
        items.add( new EliseusItem("Software::Outros::Verificar", "software__outros__verificar"));
        items.add( new EliseusItem("Software::SiMagic (Selos)::Configurar", "software__simagic__selos___configurar"));
        items.add( new EliseusItem("Software::SiMagic (Selos)::Instalar", "software__simagic__selos___instalar"));
        items.add( new EliseusItem("Software::SiMagic (Selos)::Verificar", "software__simagic__selos___verificar"));
        items.add( new EliseusItem("Software::Skype::Configurar", "software__skype__configurar"));
        items.add( new EliseusItem("Software::Skype::Instalar", "software__skype__instalar"));
        items.add( new EliseusItem("Software::Skype::Verificar", "software__skype__verificar"));
        items.add( new EliseusItem("Software::TeamViewer::Configurar", "software__teamviewer__configurar"));
        items.add( new EliseusItem("Software::TeamViewer::Instalar", "software__teamviewer__instalar"));
        items.add( new EliseusItem("Software::TeamViewer::Verificar", "software__teamviewer__verificar"));
        items.add( new EliseusItem("Software::VNC::Configurar", "software__vnc__configurar"));
        items.add( new EliseusItem("Software::VNC::Instalar", "software__vnc__instalar"));
        items.add( new EliseusItem("Software::VNC::Verificar", "software__vnc__verificar"));
        items.add( new EliseusItem("Software::Windows::Configurar", "software__windows__configurar"));
        items.add( new EliseusItem("Software::Windows::Instalar", "software__windows__instalar"));
        items.add( new EliseusItem("Software::Windows::Verificar", "software__windows__verificar"));
        items.add( new EliseusItem("Telefonia::Central Telefônica::Alterar DDR", "telefonia__central_telefônica__alterar_ddr"));
        items.add( new EliseusItem("Telefonia::Central Telefônica::Bloquear DDD/DDI", "telefonia__central_telefônica__bloquear_ddd_ddi"));
        items.add( new EliseusItem("Telefonia::Central Telefônica::Criar DDR", "telefonia__central_telefônica__criar_ddr"));
        items.add( new EliseusItem("Telefonia::Central Telefônica::Criar grupo", "telefonia__central_telefônica__criar_grupo"));
        items.add( new EliseusItem("Telefonia::Central Telefônica::Liberar DDD/DDI", "telefonia__central_telefônica__liberar_ddd_ddi"));
        items.add( new EliseusItem("Telefonia::Central Telefônica::Regra de desvio", "telefonia__central_telefônica__regra_de_desvio"));
        items.add( new EliseusItem("Telefonia::Central Telefônica::Verificar cellfix", "telefonia__central_telefônica__verificar_cellfix"));
        items.add( new EliseusItem("Telefonia::Ramal::Configurar ramal", "telefonia__ramal__configurar_ramal"));
        items.add( new EliseusItem("Telefonia::Ramal::Criar ramal", "telefonia__ramal__criar_ramal"));
        items.add( new EliseusItem("Telefonia::Ramal::Incluir no grupo", "telefonia__ramal__incluir_no_grupo"));
        items.add( new EliseusItem("Telefonia::Ramal::Remanejar ramal", "telefonia__ramal__remanejar_ramal"));
        items.add( new EliseusItem("Telefonia::Ramal::Remover do grupo", "telefonia__ramal__remover_do_grupo"));
        items.add( new EliseusItem("Telefonia::Ramal::Verificar ramal", "telefonia__ramal__verificar_ramal"));
        items.add( new EliseusItem("Telefonia::Smartphone::Configurar", "telefonia__smartphone__configurar"));
        items.add( new EliseusItem("Telefonia::Smartphone::Resetar", "telefonia__smartphone__resetar"));
        items.add( new EliseusItem("Telefonia::Smartphone::Solicitar", "telefonia__smartphone__solicitar"));
        items.add( new EliseusItem("Telefonia::URA::Incluir itens no menu", "telefonia__ura__incluir_itens_no_menu"));
        items.add( new EliseusItem("Telefonia::URA::Música de espera", "telefonia__ura__música_de_espera"));
        items.add( new EliseusItem("Telefonia::URA::Remover itens do menu", "telefonia__ura__remover_itens_do_menu"));
        items.add( new EliseusItem("Telefonia::URA::Verificar fila de espera", "telefonia__ura__verificar_fila_de_espera"));
        items.add( new EliseusItem("Telefonia::URA::Verificar menu", "telefonia__ura__verificar_menu"));

        ArrayList<ITreeObject> tree = new ArrayList<ITreeObject>();
        MyTreeObject parent = null;
        for(EliseusItem eliseu : items){
            String[] levels = eliseu.levels.split("::");
            for(int i = 0; i< levels.length; i++){
                MyTreeObject node = new MyTreeObject();
                node.setItem(levels[i]);
                if( i == levels.length-1)
                    node.setValue(eliseu.value);
                if(parent != null) {
                    if(!parent.getChildren().contains(node)) {
                        parent.addChild(node);
                        node.setParent(parent);
                    }else{
                        node = (MyTreeObject)parent.getChildren().get(parent.getChildren().indexOf(node));
                    }

                }else {
                    if (!tree.contains(node)) {
                        tree.add(node);
                    }else{
                        node = (MyTreeObject)tree.get(tree.indexOf(node));
                    }
                }
                parent = node;

            }


            parent = null;

        }



        treeNavigationView3.setTreeObjectList(tree);

        treeNavigationView.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(ITreeObject item) {
                Toast.makeText(context, item.getItem(), Toast.LENGTH_SHORT).show();
            }
        });
        treeNavigationView2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(ITreeObject item) {
                Toast.makeText(context, item.getItem(), Toast.LENGTH_SHORT).show();
            }
        });
        treeNavigationView3.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(ITreeObject item) {Toast.makeText(context , item.getItem() + "\n" + item.getValue(), Toast.LENGTH_SHORT).show();}
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        treeNavigationView.collapse();
        treeNavigationView2.collapse();
        treeNavigationView3.collapse();
    }

    private class EliseusItem{
        public String levels;
        public String value;

        public EliseusItem(String levels, String value){
            this.levels = levels;
            this.value = value;
        }
    }
}
