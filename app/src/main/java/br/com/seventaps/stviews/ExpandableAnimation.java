package br.com.seventaps.stviews;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by Joao on 24/06/2015.
 */
public class ExpandableAnimation extends Animation{
    private final int mDeltaHeight;
    private final int mCurrentHeight;
    private final View view;

    public ExpandableAnimation(View view, int currentHeight, int targetHeight) {
        this.view = view;
        mCurrentHeight = currentHeight;
        mDeltaHeight = targetHeight - currentHeight;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        view.getLayoutParams().height = (int)(mCurrentHeight + mDeltaHeight * interpolatedTime);
        view.requestLayout();
    }

    @Override
    public void initialize(int width, int height, int parentWidth,
                           int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}
