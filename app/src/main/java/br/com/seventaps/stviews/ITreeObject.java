package br.com.seventaps.stviews;

import java.util.ArrayList;

/**
 * Created by Joao on 24/06/2015.
 */
public interface ITreeObject {
    public void setValue(String value);
    public String getValue();
    public void setItem(String item);
    public String getItem();
    public ArrayList<ITreeObject> getChildren();
    public void setChildren(ArrayList<ITreeObject> children);
    public ITreeObject getParent();
    public void setParent(ITreeObject parent);
}
