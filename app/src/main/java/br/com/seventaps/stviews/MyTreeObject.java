package br.com.seventaps.stviews;

import android.support.annotation.NonNull;

import java.util.ArrayList;

/**
 * Created by Joao on 24/06/2015.
 */
public class MyTreeObject implements ITreeObject{

    String value;
    String item;
    ArrayList<ITreeObject> children;
    ITreeObject parent;


    public MyTreeObject(){
        children = new ArrayList<>();
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public void addChild(MyTreeObject child) {
        child.setParent(this);
        this.children.add(child);
    }

    public String getItem() {
        return item;
    }

    public ArrayList<ITreeObject> getChildren() {
        if(children == null)
            children = new ArrayList<>();
        return children;
    }

    public void setChildren(ArrayList<ITreeObject> children) {
        for(ITreeObject child : children){
            child.setParent(this);
        }
        this.children = children;
    }

    public ITreeObject getParent() {
        return parent;
    }

    public void setParent(ITreeObject parent){
        this.parent = parent;
    }


    @Override
    public boolean equals(Object o) {
        boolean sameSame = false;

        if (o != null && o instanceof MyTreeObject)
        {
            sameSame = this.item.equals((( MyTreeObject) o).item);
        }

        return sameSame;
    }
}
