package br.com.seventaps.stviews;

/**
 * Created by Joao on 24/06/2015.
 */
public interface OnItemSelectedListener {
    public void onItemSelected(ITreeObject item);
}
