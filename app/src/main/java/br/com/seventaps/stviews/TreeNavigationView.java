package br.com.seventaps.stviews;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by joaortk on 23/06/2015.
 */
public class TreeNavigationView extends ScrollView {
    private float mTextSize;
    private float mRowHeight;
    private int mTextColor;
    private int mNodeBackground;
    private int mNodeNextIcon;
    private ArrayList<ITreeObject> treeObjectList;
    boolean collapsed = false;



    private OnItemSelectedListener mListener;
    private Canvas canvas;
    RelativeLayout itemRow;
    ITreeObject selected;
    String backTextIndicator;
    String colapsedDefaultText;
    private int mAnimationDuration = 5000;
    LinearLayout mContent;

    public TreeNavigationView(Context context) {
        super(context);
    }

    public TreeNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.TreeNavigationView,
                0, 0);

        try {
            mTextSize = a.getDimension(R.styleable.TreeNavigationView_textSize, 12);
            backTextIndicator = a.getString(R.styleable.TreeNavigationView_backIndicatorText);
            if(backTextIndicator ==null || backTextIndicator.isEmpty())
                backTextIndicator = "Back";
            colapsedDefaultText = a.getString(R.styleable.TreeNavigationView_colapsedDefaultText);
            if(colapsedDefaultText ==null || colapsedDefaultText.isEmpty())
                colapsedDefaultText = "Select";
            mRowHeight = a.getDimension(R.styleable.TreeNavigationView_rowHeight, mTextSize);
            mTextColor = a.getColor(R.styleable.TreeNavigationView_nodeTextColor, R.color.primary_text_default_material_light);
            mNodeBackground = a.getResourceId(R.styleable.TreeNavigationView_nodeBackground, R.drawable.abc_list_selector_holo_light);
            mNodeNextIcon = a.getResourceId(R.styleable.TreeNavigationView_nodeNextIcon, R.drawable.ic_next);

        } finally {
            a.recycle();
        }
        init();

    }

    private void init() {
        if(treeObjectList == null)
            treeObjectList = new ArrayList<ITreeObject>();
        mContent = new LinearLayout(getContext());
        mContent.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    expand();
                else
                    collapse();
            }
        });
        mContent.setOrientation(LinearLayout.VERTICAL);
        this.addView(mContent);
        mContent.setClickable(true);
        mContent.setFocusable(true);
        mContent.setFocusableInTouchMode(true);
    }

    public TreeNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    public int getmTextColor() {
        return mTextColor;
    }

    public void setmTextColor(int mTextColor) {
        this.mTextColor = mTextColor;
        invalidate();
    }

    public int getmNodeBackground() {
        return mNodeBackground;
    }

    public void setmNodeBackground(int mNodeBackground) {
        this.mNodeBackground = mNodeBackground;
    }

    public ArrayList<ITreeObject> getTreeObjectList() {
        return treeObjectList;
    }

    public void setTreeObjectList(ArrayList<ITreeObject> treeObjectList) {
        this.treeObjectList = treeObjectList;
        updateTreeObjectList(treeObjectList);
    }

    private void selected(ITreeObject item) {
        requestFocus();
        if(collapsed)
            expand();
        if(item == null){
            updateTreeObjectList(treeObjectList);
        }else{
            if(item.getChildren()!= null && item.getChildren().size() >0){
                selected = item;
                updateTreeObjectList(item.getChildren());
            }else{
                selected = item;
                mContent.clearFocus();
                notifyListener();
                if(!collapsed)
                    collapse();

            }
        }
    }

    public void collapse() {
        if(!collapsed) {
            int rows = 1;
            if (selected != null) {
                if (selected.getParent() != null)
                    rows += selected.getParent().getChildren().size();
            } else {
                rows = treeObjectList.size();
            }
            rows = 5;
            ExpandableAnimation a = new ExpandableAnimation(this, rows * (int) mRowHeight, (int) mRowHeight);
            a.setDuration(500);
            a.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mContent.removeAllViews();
                    //mContent.setLayoutParams(getLayoutParams());
                    if (selected != null) {
                        itemRow = createItem(selected.getItem(), selected.getChildren().size() == 0);
                        itemRow.setTag(selected);
                        itemRow.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selected(selected.getParent());
                            }
                        });
                        mContent.addView(itemRow);
                        invalidate();
                    } else {
                        itemRow = createItem(colapsedDefaultText, true);
                        itemRow.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selected(null);
                            }
                        });
                        mContent.addView(itemRow);
                    }

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            startAnimation(a);
            collapsed = true;
        }
    }
    public  void expand() {
        if(collapsed) {
            int rows = 1;
            if (selected != null) {
                if (selected.getParent() != null)
                    rows += selected.getParent().getChildren().size();
            } else {
                rows = treeObjectList.size();
            }
            rows = 5;
            ExpandableAnimation a = new ExpandableAnimation(this, (int) mRowHeight, rows * (int) mRowHeight);
            a.setDuration(500);
            startAnimation(a);
            collapsed = false;
        }
    }

    private void notifyListener() {
        if(mListener!=null)
            mListener.onItemSelected(selected);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }




    private void updateTreeObjectList(ArrayList<ITreeObject> list) {
        mContent.removeAllViews();
        //mContent.setLayoutParams(getLayoutParams());
        if (list != null && list.size() > 0) {
            if (list.get(0).getParent() != null) {
                itemRow = createItem(backTextIndicator, true);
                itemRow.setTag(list.get(0));
                itemRow.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selected(((ITreeObject) v.getTag()).getParent().getParent());
                    }
                });
                mContent.addView(itemRow);
            }
        }
        for (ITreeObject item : list) {
            itemRow = createItem(item.getItem(), item.getChildren().size() == 0);
            itemRow.setTag(item);
            itemRow.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    selected((ITreeObject) v.getTag());
                }
            });
            mContent.addView(itemRow);
        }
        invalidate();
    }

    private RelativeLayout createItem(String text, boolean isLeaf) {
        RelativeLayout row = new RelativeLayout(getContext());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT );
        row.setLayoutParams(params);
        row.setBackgroundResource(mNodeBackground);

        TextView item = new TextView(getContext());
        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT );
        Resources r = getResources();
        float pxMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, r.getDisplayMetrics());
        textParams.setMargins((int) pxMargin, 0, (int) pxMargin, 0);
        item.setLayoutParams(textParams);
        item.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
        item.setTextColor(mTextColor);
        item.setText(text);
        item.setHeight((int) mRowHeight);
        item.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);

        row.addView(item);

        if(!isLeaf) {
            ImageView icon = new ImageView(getContext());
            icon.setImageResource(mNodeNextIcon);
            RelativeLayout.LayoutParams iconParams = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            iconParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            iconParams.addRule(RelativeLayout.CENTER_VERTICAL);
            iconParams.setMargins(0, 0, (int) pxMargin, 0);
            icon.setLayoutParams(iconParams);
            row.addView(icon);
        }
        LinearLayout divider = new LinearLayout(getContext());
        divider.setBackgroundResource(R.color.background_material_dark);
        RelativeLayout.LayoutParams dividerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 1);
        dividerParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        divider.setLayoutParams(dividerParams);

        row.addView(divider);
        return row;
    }

    public ITreeObject getSelectedItem(){
        return selected;
    }
    public void setOnItemSelectedListener(OnItemSelectedListener mListener) {
        this.mListener = mListener;
    }




}
